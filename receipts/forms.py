from django import forms
from django.forms import ModelForm
from django.db import models
from receipts.models import Receipt, ExpenseCategory, User, Account


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = ["vendor", "total", "tax", "date", "category", "account"]


class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]


class CreateAccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
        ]
