from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, CreateAccountForm

# Create your views here.


def list_receipt(request):
    # if user is authenticated,
    # logged in user- request.user dir.
    if request.user.is_authenticated:
        list_receipt_all = Receipt.objects.filter(purchaser=request.user)
        context = {"list_receipt_all": list_receipt_all}
        return render(request, "list.html", context)
    else:
        return redirect("login")


# else: login sayfasina yonlendir

# ["vendor", "total", "tax", "category", "account"]


def create_receipt(request):
    # person must be logged in to see the view.
    if not request.user.is_authenticated:
        return redirect("login")

    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            # commit=false demek save et ama database e yollama,a hafizasinda tutuyor, ama database e yollama isini erteliyor.
            saved_receipt = form.save(commit=False)
            saved_receipt.purchaser = request.user
            saved_receipt.save()
            return redirect("home")
    else:
        # bos form olustur, kullanciiya goster
        form = ReceiptForm()
    # yukarida girilen bilgileri tutup gonderiyorsun.
    context = {
        "form": form,
    }
    return render(request, "create.html", context)


def create_category(request):
    if not request.user.is_authenticated:
        return redirect("login")
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            saved_category = form.save(commit=False)
            saved_category.owner = request.user
            # request.user; logged in user demektir.
            saved_category.save()
            return redirect("category_list")
        # redirect ("...") parantez icine, hangi sayfaya yonlendireceksen
        # o sayfanin urls.py daki name'i neyse onu yaziyorsun.
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


# return icinde render edilen yere; categories/create  + .html yaziyorsun ->
# Browser da sunu istediginde: receipts/categories/create/


# su anda templates ile ayni seviyede oldugun icin,
# gidecegin yeri belirlerken categories i de basina ekliyorsun


# expense category leri listeliyoruz.
# hangi kategoriden ne kadar receipt var,
# tabloda onu gosterecegiz
# ExpenseCategory modelinden yola cikarak
def list_expense_categories(request):
    if not request.user.is_authenticated:
        return redirect("login")
    expense_categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "list_expense_categories": expense_categories,
    }
    return render(request, "categories/list.html", context)


def list_accounts(request):
    if not request.user.is_authenticated:
        return redirect("login")
    account_categories = Account.objects.filter(owner=request.user)
    context = {"list_accounts": account_categories}
    return render(request, "accounts/list.html", context)


def create_account(request):
    if not request.user.is_authenticated:
        return redirect("login")

    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            saved_account = form.save(commit=False)
            saved_account.owner = request.user
            saved_account.save()
            return redirect("account_list")
    else:
        form = CreateAccountForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)


# redirect ("...") parantez icine, hangi sayfaya yonlendireceksen
# o sayfanin urls.py daki name'i neyse onu yaziyorsun.
