from django.urls import path
from receipts.views import (
    list_receipt,
    create_receipt,
    list_expense_categories,
    list_accounts,
    create_category,
    create_account,
)

# buradaki path lerin hepsi receipts file inin icinde oldugu icin,
# projedeki urls.py icinde (expenses/ursl.py)
# basinda receipts/ olacak sekilde kaydediliyor.

# Proje yonergesinde "register that view for
# the path "...." in the receipts.urls.py and the name "create_category"diyor

urlpatterns = [
    path("", list_receipt, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("list/", list_receipt, name="list_receipt"),
    path("categories/", list_expense_categories, name="category_list"),
    path("accounts/", list_accounts, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]

# categories/create sonuna "/" eklemezsen yanlis oluyor.


# path("create/", create_receipt, name="create"),
# http://localhost:8000/receipts/categories  -> receipts/categories
# http://localhost:8000/receipts/list/


# receipts/ accounts

# path("accounts", list_accounts, name="account_list"),

""" localhost/m/ abcs
burasi include("m.urls") deki m.urls. bu dosyanin adi o.
gelince path (" m ", include ("m.urls")   )

simdi abcs burada var mi diye bakacagiz.
buradaki abcs yi path deki ilk sirada arayacagiz. eger varsa,
devaminda, view fonksiyonu yaziyor, ona gidecegiz. eger varsa adi var,
(name = "... ") onu da base.html de ekleyecegiz ki cagirabilelim-
<nav> kisminda

 """
