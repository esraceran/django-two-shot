"""expenses URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect


def redirect_to_list(request):
    return redirect("home")


# In the expenses urls.py, write a function that
# redirects to the name of the path for the list view
#  that you created in the previous feature.


urlpatterns = [
    path("", redirect_to_list, name="root"),
    path("admin/", admin.site.urls),
    # receipts/urls.py URL patterns are included here.
    path("receipts/", include("receipts.urls")),
    # URL patterns from accounts/urls.py are included here.
    path("accounts/", include("accounts.urls")),
]


# path (" m ", include ("m.urls")   )

# localhost/m/ abcs
# m i aliyor html den, ve projenin URL lerinin oldugu dosyaya, yani buraya geliyor.
# eger m path lerin basinda varsa, include edilmis kisma gidip devami var mi diye bakiyor.
# include edilen yer dosyanin adi. ve orasi da app lerin url. i. applerin url ine gidince, orada var mi diye bakiyor.
